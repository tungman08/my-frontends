import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import toast, { PluginOptions, POSITION } from 'vue-toastification';
import './assets/styles/global.scss';

// 3rd party plugins.
import 'bootstrap';
import * as dataTables from 'datatables.net-bs5';

const options: PluginOptions = { position: POSITION.BOTTOM_RIGHT };
dataTables(window, $);

createApp(App)
  .use(store)
  .use(router)
  .use(toast, options)
  .mount('#app');
