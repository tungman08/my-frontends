import { defineComponent, onMounted, ref } from 'vue';
import { useTodoStore } from '@/store/todo/todo.store';
import { Todo } from '@/store/todo/todo.model';
import TodoTableComponent from '@/components/todoTable/TodoTableComponent.vue';
import TodoModalComponent from '@/components/todoModal/TodoModalComponent.vue';

export default defineComponent({
  name: 'TodoPage',
  components: {
    TodoTableComponent,
    TodoModalComponent,
  },
  setup: () => {
    const store = useTodoStore();
    const todo = ref<Todo>();
    const todoModal = ref<InstanceType<typeof TodoModalComponent>>();

    onMounted(() => {
      store.fetch();
    });

    const openCreateModal = () => {
      todo.value = undefined;
      todoModal.value?.show();
    };

    const openEditModal = (item: Todo) => {
      todo.value = item;
      todoModal.value?.show();
    };

    return {
      todo,
      todoModal,
      openCreateModal,
      openEditModal,
    };
  },
});
