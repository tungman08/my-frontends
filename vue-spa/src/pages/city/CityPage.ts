import { defineComponent, onMounted, ref, watch } from 'vue';
import { useWeatherStore } from '@/store/weather/weather.store';
import WeatherComponent from '@/components/weather/WeatherComponent.vue';
import GoBackComponent from '@/components/goBack/GoBackComponent.vue';
import fall from '@/assets/images/fall.jpg';
import spring from '@/assets/images/spring.jpg';
import summer from '@/assets/images/summer.jpg';
import winter from '@/assets/images/winter.jpg';

export default defineComponent({
  name: 'CityPage',
  components: {
    WeatherComponent,
    GoBackComponent,
  },
  setup: () => {
    const store = useWeatherStore();
    const city = store.city;
    const status = store.status;
    const search = ref('');
    const cityName = ref(search.value);
    const background = ref('');

    onMounted(() => {
      search.value = 'bangkok';
    });

    watch(search, (search) => {
      store.getCity(search);
    });

    watch(status, (status) => {
      if (status === 'idle') {
        cityName.value = search.value;
        background.value = getBackground(city.value?.main.temp ?? 0);
      }
    });

    const setSearch = (name: string) => {
      search.value = name;
    };

    const goBack = () => {
      search.value = cityName.value;
    };

    const getBackground = (temperature: number) => {
      const bg = (temperature > 0)
        ? (temperature > 10)
          ? (temperature > 20)
            ? summer
            : spring
          : fall
        : winter;
      return `url(${bg})`;
    };

    return {
      city,
      status,
      background,
      setSearch,
      goBack,
    };
  }
});
