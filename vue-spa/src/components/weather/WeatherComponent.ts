import { defineComponent, PropType, ref } from 'vue';
import { useWeatherStore } from '@/store/weather/weather.store';
import type { City } from '@/store/weather/weather.model';

export default defineComponent({
  name: 'WeatherComponent',
  props: {
    city: {
      type: Object as PropType<City>,
      required: true,
    },
  },
  emits: [
    'search'
  ],
  setup: (_, context) => {
    const store = useWeatherStore();
    const status = store.status;
    const input = ref('');
    const today = new Date();

    const searchClick = () => {
      if (input.value.length !== 0) {
        const formatedSearch = input.value.trim().toLowerCase().replace(/\s\s+/g, ' ').split(' ').join('+');
        input.value = '';
        context.emit('search', formatedSearch);
      }
    };

    const weatherIcon = (weathers?: Array<{ id: number; main: string; description: string; icon: string; }>) => {
      const icon = (weathers)
      ? weathers.length !== 0
        ? weathers[0].icon
        : '01d'
      : '01d';
      return `https://openweathermap.org/img/wn/${icon}@4x.png`;
    };

    const weatherName = (weathers?: Array<{ id: number; main: string; description: string; icon: string; }>) => {
      return (weathers)
        ? (weathers.length !== 0)
          ? weathers[0].main
          : ''
        : '';
    };

    return {
      status,
      today,
      input,
      searchClick,
      weatherIcon,
      weatherName,
    };
  }
});
