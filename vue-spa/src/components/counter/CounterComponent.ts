import { defineComponent } from 'vue';
import { useCounterStore } from '@/store/counter/counter.store';

export default defineComponent({
  name: 'CounterComponent',
  setup: () => {
    const store = useCounterStore();
    const count = store.counter;

    const incrementClick = () => {
      store.increment();
    };

    const decrementClick = () => {
      store.decrement();
    };

    return {
      count,
      incrementClick,
      decrementClick,
    };
  }
});
