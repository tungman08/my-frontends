import { AxiosResponse } from 'axios';
import { useAxios } from '@/composables/axios';
import type { City } from './weather.model';

const axios = useAxios('https://api.openweathermap.org/data/2.5');
const apiKey = import.meta.env.VITE_API_KEY;

export interface WeatherService {
  getCity: (cityName: string) => Promise<AxiosResponse<City>>;
}

const getCity = (cityName: string): Promise<AxiosResponse<City>> =>
  axios.Get<City>(`/weather?q=${cityName}&units=metric&appid=${apiKey}`);

export default { getCity } as WeatherService;
