import { createPinia } from 'pinia';

export type Status = 'idle' | 'loading' | 'failed';
export default createPinia();
