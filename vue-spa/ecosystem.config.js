module.exports = {
  apps: [
    {
      name: 'VueSPA',
      script: 'serve',
      exec_mode: 'cluster',
      instances: 'max',
      env: {
        PM2_SERVE_PATH: 'dist',
        PM2_SERVE_PORT: 3000,
        PM2_SERVE_SPA: 'true',
        PM2_SERVE_HOMEPAGE: '/index.html'
      }
    }
  ]
};
