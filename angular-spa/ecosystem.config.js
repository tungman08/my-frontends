module.exports = {
  apps: [
    {
      name: 'AngularSPA',
      script: 'serve',
      exec_mode: 'cluster',
      instances: 'max',
      env: {
        PM2_SERVE_PATH: 'dist',
        PM2_SERVE_PORT: 4200,
        PM2_SERVE_SPA: 'true',
        PM2_SERVE_HOMEPAGE: '/index.html'
      }
    }
  ]
};
