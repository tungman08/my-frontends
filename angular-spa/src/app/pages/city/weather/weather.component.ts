import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import type { State, Status } from '@/app/store';
import { selectStatus } from '@/app/store/weather/weather.selectors';
import type { City, Weather } from '@/app/store/weather/weather.model';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent {
  @Input() public city?: City;
  @Output() public search = new EventEmitter<string>();
  public status$: Observable<Status>;
  public today: number = Date.now();
  public input: string = '';

  constructor(private store: Store<State>,
              private sanitizer: DomSanitizer) {

    this.status$ = this.store.select(selectStatus);
  }

  public searchClick()  {
    if (this.input.length !== 0) {
      const formatedSearch = this.input.trim().toLowerCase().replace(/\s\s+/g, ' ').split(' ').join('+');
      this.input = '';
      this.search.emit(formatedSearch);
    }
  }

  public weatherIcon(weathers?: Weather[]): SafeUrl {
    const icon = (weathers)
      ? weathers.length !== 0
        ? weathers[0].icon
        : '01d'
      : '01d';
    return this.sanitizer.bypassSecurityTrustUrl(`https://openweathermap.org/img/wn/${icon}@4x.png`);
  }

  public weatherName(weathers?: Weather[]): string {
    return (weathers)
      ? (weathers.length !== 0)
        ? weathers[0].main
        : ''
      : '';
  };
}
