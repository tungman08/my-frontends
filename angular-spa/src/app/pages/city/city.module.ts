import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CityRoutingModule } from './city-routing.module';
import { CityComponent } from './city.component';
import { WeatherComponent } from './weather/weather.component';
import { GoBackComponent } from './go-back/go-back.component';


@NgModule({
  declarations: [
    CityComponent,
    WeatherComponent,
    GoBackComponent
  ],
  imports: [
    CommonModule,
    CityRoutingModule,
    FormsModule
  ]
})
export class CityModule { }
