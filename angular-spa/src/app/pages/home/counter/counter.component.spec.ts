import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { Store } from '@ngrx/store';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { CounterComponent } from './counter.component';
import { State } from '@/app/store';

describe('CounterComponent', () => {
  let component: CounterComponent;
  let fixture: ComponentFixture<CounterComponent>;
  let mockStore: MockStore<State>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CounterComponent ],
      providers: [ provideMockStore() ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(CounterComponent);
    component = fixture.componentInstance;
    mockStore = TestBed.inject(Store) as MockStore<any>;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should increment the counter value when increment is clicked', fakeAsync(() => {
    spyOn(component, 'incrementClick');
    buttonClick('[aria-label="Increment value"]');
    tick();

    expect(component.incrementClick).toHaveBeenCalled();
  }));

  it('should decrement the counter value when decrement is clicked', fakeAsync(() => {
    spyOn(component, 'decrementClick');
    buttonClick('[aria-label="Decrement value"]');
    tick();

    expect(component.decrementClick).toHaveBeenCalled();
  }));

  function buttonClick(selector: string) {
    const button = fixture.debugElement.nativeElement.querySelector(selector);
    button.click();
  }
});
