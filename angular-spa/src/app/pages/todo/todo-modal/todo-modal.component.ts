import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import type { State, Status } from '@/app/store';
import * as todoAction from '@/app/store/todo/todo.actions';
import { selectStatus } from '@/app/store/todo/todo.selectors';
import { Todo } from '@/app/store/todo/todo.model';
import { TodoFormComponent } from '../todo-form/todo-form.component';

@Component({
  selector: 'app-todo-modal',
  templateUrl: './todo-modal.component.html',
  styleUrls: ['./todo-modal.component.scss']
})
export class TodoModalComponent implements OnInit  {
  @Input() public modalRef?: BsModalRef;
  @ViewChild('todoForm', { static: false }) private todoForm?: TodoFormComponent;
  public todo?: Todo;
  public isEditMode: boolean = false;
  public title: string = '';
  public icon: string = '';
  public status$: Observable<Status>;

  constructor(private modalService: BsModalService,
    private store: Store<State>) {

    this.todo = this.modalService.config.initialState as Todo | undefined;
    this.status$ = this.store.select(selectStatus);
  }

  ngOnInit(): void {
    this.isEditMode = !!this.todo;
    this.title = this.isEditMode ? 'Edit a todo item' : 'New a todo item';
    this.icon = this.isEditMode ? 'fa-pen' : 'fa-plus';
  }

  submit(): void {
    this.todoForm?.validate();
  }

  save(formData: Todo): void {
    if (this.isEditMode) {
      this.update(formData);
    } else {
      this.create(formData);
    }
  }

  create(formData: Todo): void {
    Promise.resolve(this.store.dispatch(todoAction.createTodo(formData)))
      .then(() => this.close());
  }

  update(formData: Todo): void {
    Promise.resolve(this.store.dispatch(todoAction.updateTodo(formData)))
      .then(() => this.close());
  }

  remove(): void {
    Promise.resolve(this.store.dispatch(todoAction.deleteTodo({ id: this.todo?.id as number })))
      .then(() => this.close());
  }

  open(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }

  close(): void {
    this.modalRef?.hide();
  }
}
