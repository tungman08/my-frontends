import { AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import type { State } from '@/app/store';
import { selectTodos } from '@/app/store/todo/todo.selectors';
import type { Todo } from '@/app/store/todo/todo.model';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-todo-table',
  templateUrl: './todo-table.component.html',
  styleUrls: ['./todo-table.component.scss']
})
export class TodoTableComponent implements OnInit, OnDestroy, AfterViewInit {
  @Output() public selected = new EventEmitter<Todo>();
  @ViewChild(DataTableDirective, { static: false }) private dtElement?: DataTableDirective;
  public dtTrigger: Subject<any> = new Subject();
  public dtOptions: DataTables.Settings = {};
  public todos$: Observable<Todo[]>;
  public subscription: Subscription;
  public todos: Todo[] = [];

  constructor(private store: Store<State>) {
    this.todos$ = this.store.select(selectTodos);
    this.subscription = this.todos$.subscribe((todos) => {
      this.todos = todos;
      this.rerender();
    });
  }

  ngOnInit(): void {
    this.dtOptions = {
      pageLength: 25,
      autoWidth: false,
      columns: [
        { width: '10%' },
        { width: '30%' },
        { width: '30%' },
        { width: '30%' },
      ],
    };
  }

  ngAfterViewInit(): void {
    // Call the dtTrigger to render dataTable
    this.dtTrigger.next(this.dtOptions);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.dtTrigger.unsubscribe();
  }

  selectedItem(todo: Todo): void {
    this.selected.emit(todo);
  }

  rerender(): void {
    const dtInstance = this.dtElement?.dtInstance;

    if (dtInstance) {
      dtInstance.then((dt: DataTables.Api) => {
        // Destroy the table first
        dt.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next(this.dtOptions);
      });
    }
  }
}
