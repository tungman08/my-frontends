import { createReducer, on } from '@ngrx/store';
import { Status } from '..';
import * as todoAction from './todo.actions';
import { Todo } from './todo.model';

// feature name
export const TODO_FEATURE_NAME = 'todoStore';

// state
export interface TodoState {
  todos: Todo[];
  status: Status;
}

const initialState: TodoState = {
  todos: [],
  status: 'idle',
};

// reducer
export const todoReducer = createReducer(
  initialState,
  on(todoAction.fetchTodoSuccess,
    (state, { todos }): TodoState => ({ ...state, todos })
  ),
  on(todoAction.createTodoSuccess,
    (state, { todo }): TodoState => {
      const todos = [...state.todos];
      todos.push(todo);
      return { ...state, todos };
    }
  ),
  on(todoAction.updateTodoSuccess,
    (state, { todo }): TodoState => {
      const todos = [...state.todos];
      const index = state.todos.findIndex(m => m.id === todo.id);
      todos[index] = todo;
      return { ...state, todos };
    }
  ),
  on(todoAction.deleteTodoSuccess,
    (state, { id }): TodoState => {
      const todos = [...state.todos];
      const index = state.todos.findIndex(m => m.id === id);
      todos.splice(index, 1);
      return { ...state, todos };
    }
  ),
  on(todoAction.fetchTodoSuccess,
    todoAction.createTodoSuccess,
    todoAction.updateTodoSuccess,
    todoAction.deleteTodoSuccess,
    (state): TodoState => ({ ...state, status: 'idle', })
  ),
  on(todoAction.fetchTodo,
    todoAction.createTodo,
    todoAction.updateTodo,
    todoAction.deleteTodo,
    (state): TodoState => ({ ...state, status: 'loading', })
  ),
  on(todoAction.fetchTodoError,
    todoAction.createTodoError,
    todoAction.updateTodoError,
    todoAction.deleteTodoError,
    (state): TodoState => ({ ...state, status: 'failed' })
  ),
);
