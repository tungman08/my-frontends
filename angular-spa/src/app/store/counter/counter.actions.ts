import { createAction, props } from '@ngrx/store';

enum CounterTypes {
  INCREMENT = '[Counter] Increment a value',
  DECREMENT = '[Counter] Decrement a value',
  SET_AMOUNT = '[Counter] Set a counter value',
}

export const increment = createAction(CounterTypes.INCREMENT);
export const decrement = createAction(CounterTypes.DECREMENT);
export const setAmount = createAction(CounterTypes.SET_AMOUNT, props<{ amount: number }>());
