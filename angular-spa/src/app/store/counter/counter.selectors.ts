import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CounterState, COUNTER_FEATURE_NAME } from './counter.reducer';

export const selectCounterState = createFeatureSelector<CounterState>(COUNTER_FEATURE_NAME);

export const selectCounter = createSelector(
  selectCounterState, (state: CounterState) => state.counter
);
