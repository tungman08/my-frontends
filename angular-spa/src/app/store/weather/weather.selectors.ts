import { createFeatureSelector, createSelector } from '@ngrx/store';
import { WeatherState, WEATHER_FEATURE_NAME } from './weather.reducer';

export const selectWeatherState = createFeatureSelector<WeatherState>(WEATHER_FEATURE_NAME);

export const selectCity = createSelector(
  selectWeatherState, (state: WeatherState) => state.city
);

export const selectStatus = createSelector(
  selectWeatherState, (state: WeatherState) => state.status
);
