import { createReducer, on } from '@ngrx/store';
import { Status } from '..';
import * as weatherAction from './weather.actions';
import { City } from './weather.model';

// feature name
export const WEATHER_FEATURE_NAME = 'weatherStore';

// state
export interface WeatherState {
  city: City;
  status: Status;
}

const initialState: WeatherState = {
  city: {} as City,
  status: 'idle',
};

// reducer
export const weatherReducer = createReducer(
  initialState,
  on(weatherAction.getCity, (state): WeatherState => ({
    ...state,
    status: 'loading'
  })),
  on(weatherAction.getCitySuccess, (state, { city }): WeatherState => ({
    city,
    status: 'idle'
  })),
  on(weatherAction.getCityError, (state): WeatherState => ({
    ...state,
    status: 'failed'
  })),
);
