import React from 'react';
import { Link, NavLink, Outlet } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import logo from '@/assets/images/logo.svg';
import styles from './LayoutComponent.module.scss';

const LayoutComponent = (): JSX.Element => {
  return (
    <div className={styles.layout}>
      <header>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container-fluid">
            <Link className="navbar-brand" to="/">
              <img
                src={logo}
                width="30"
                height="30"
                className="d-inline-block align-top me-2"
                alt="logo"
              />React
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarText"
              aria-controls="navbarText"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/" end>Home</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/city">City</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/todo">Todo</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/about">About</NavLink>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>

      <Outlet />
      <ToastContainer position="bottom-right" theme="colored" />
    </div>
  );
};

export default LayoutComponent;