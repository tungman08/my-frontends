import React from 'react';
import styles from './TodoModalComponent.module.scss';
import Modal from 'react-bootstrap/Modal';
import type { Ref } from '@/components/modal/ref';
import { useAppDispatch, useAppSelector } from '@/hooks/redux';
import { TodoAction, TodoSelector } from '@/store/todo/todoSlice';
import { Todo } from '@/store/todo/todoModel';
import Swal, { SweetAlertOptions } from 'sweetalert2';
import TodoFormComponent, { Ref as FormRef } from './TodoFormComponent';

type Props = {
  todo?: Todo,
};

const TodoModalComponent = React.forwardRef<Ref, Props>((props, ref) => {
  React.useImperativeHandle(ref, () => ({
    show: handleShow,
    hide: handleClose,
  }));

  const dispatch = useAppDispatch();
  const status = useAppSelector(TodoSelector.status);
  const [show, setShow] = React.useState(false);
  const todoForm = React.useRef<FormRef>(null);
  const isEditMode = !!props.todo;
  const title = isEditMode ? 'Edit a todo item' : 'New a todo item';
  const icon = isEditMode ? 'fa-pen' : 'fa-plus';
  const options: SweetAlertOptions = {
    title: 'Delete',
    text: 'Do you want to delete this item ?',
    icon: 'warning',
    confirmButtonColor: '#f86c6b',
    showCancelButton: true,
    focusCancel: true,
  };

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const handleSubmit = async () => {
    const formData = todoForm.current?.validate();

    if (formData) {
      const action = (isEditMode)
        ? await dispatch(TodoAction.update(formData.id as number, formData.name, formData.date, formData.isDone))
        : await dispatch(TodoAction.create(formData.name, formData.date, formData.isDone));

      if (action.type.includes('fulfilled')) {
        handleClose();
      }
    }
  };

  const handleDelete = async () => {
    const result = await Swal.fire(options);

    if (result.isConfirmed) {
      const id = props.todo?.id as number;
      const action = await dispatch(TodoAction.remove(id));

      if (action.type.includes('fulfilled')) {
        handleClose();
      }
    }
  };

  return (
    <Modal
      ref={ref}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header closeButton>
        <h5 className="modal-title">
          <span className={`fas ${icon} me-2`}></span>{title}
        </h5>
      </Modal.Header>
      <Modal.Body>
        <TodoFormComponent
          ref={todoForm}
          todo={props.todo}
        />
      </Modal.Body>
      <Modal.Footer>
        {isEditMode &&
          <button
            type="button"
            className="btn btn-danger me-auto"
            disabled={status === 'loading'}
            onClick={handleDelete}
          >
            <span className="far fa-fw fa-trash-can me-2"></span>Delete
          </button>
        }

        <button
          type="button"
          className="btn btn-primary"
          disabled={status === 'loading'}
          onClick={handleSubmit}
        >
          <span className="fas fa-fw fa-save me-2"></span>Save
        </button>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={handleClose}
        >
          <span className="fas fa-fw fa-ban me-2"></span>Close
        </button>
      </Modal.Footer>
    </Modal>
  );
});

TodoModalComponent.displayName = 'TodoModal';
export default TodoModalComponent;
