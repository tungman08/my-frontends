import React from 'react';
import styles from './GoBackComponent.module.scss';

type Props = {
  onGoBack: React.MouseEventHandler<HTMLButtonElement>
};

const GoBackComponent = (props: Props): JSX.Element => {
  return (
    <div className={`${styles.panel} text-center`}>
      <h1>Oops, we can&apos;t find that city</h1>
      <button
        type="button"
        className="btn btn-primary mt-5"
        onClick={props.onGoBack}
      >
        <span className="fa-solid fa-rotate-left me-2"></span>Go Back
      </button>
    </div>
  );
};

export default GoBackComponent;
