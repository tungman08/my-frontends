import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './store';
import App from './App';
import './assets/styles/global.scss';

// 3rd party plugins.
import 'bootstrap';
import dataTables from 'datatables.net-bs5';

const container = document.getElementById('root') as HTMLElement;
const root = ReactDOM.createRoot(container);
dataTables(window, $);

root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <App />
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
);
