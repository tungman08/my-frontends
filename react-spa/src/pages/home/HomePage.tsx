import React from 'react';
import { useAppSelector } from '@/hooks/redux';
import { CounterSelector } from '@/store/counter/counterSlice';
import CounterComponent from '@/components/counter/CounterComponent';
import styles from './HomePage.module.scss';

const HomePage = (): JSX.Element => {
  const greeting = React.useRef<HTMLElement>(null);
  const count = useAppSelector(CounterSelector.counter);

  React.useEffect(() => {
    if (greeting.current) {
      $(greeting.current).html('Hello React!!');
    }
  }, []);
  
  return (
    <main className={styles.main}>
      <div className={`${styles.react} d-flex justify-content-center align-items-center`}>
        <div className="text-center">
          <h4>
            <span className="fab fa-react h4 me-2"></span>
            <span ref={greeting} className="h4"></span>
          </h4>
          <div className={`${styles['react-logo']} my-5`}></div>
          <CounterComponent />
        </div>
      </div>

      <footer className="footer fixed-bottom py-3 bg-dark">
        <div className="container d-flex flex-row-reverse">
          <div className="text-light">Counter: {count}</div>
        </div>
      </footer>
    </main>
  );
};

export default HomePage;
