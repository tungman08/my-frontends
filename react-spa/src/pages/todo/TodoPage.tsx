import React from 'react';
import styles from './TodoPage.module.scss';
import { useAppDispatch } from '@/hooks/redux';
import { TodoAction } from '@/store/todo/todoSlice';
import { Todo } from '@/store/todo/todoModel';
import type { Ref } from '@/components/modal/ref';
import TodoTableComponent from '@/components/todo/TodoTableComponent';
import TodoModalComponent from '@/components/todo/TodoModalComponent';

const TodoPage = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const todoModal = React.useRef<Ref>(null);
  const [selectedTodo, setSelectTodo] = React.useState<Todo>();

  React.useEffect(() => {
    dispatch(TodoAction.fetch());
  }, []);

  const openCreateModal = () => {
    setSelectTodo(undefined);
    todoModal.current?.show();
  };

  const openEditModal = (todo: Todo) => {
    setSelectTodo(todo);
    todoModal.current?.show();    
  };

  return (
    <main>
      <div className="container my-4">
        <div className="card">
          <div className="card-body">
            <button
              className="btn btn-primary"
              onClick={openCreateModal}
            >
              <span className="fas fa-plus me-2"></span>Add
            </button>

            <TodoTableComponent onSelected={openEditModal} />
          </div>
        </div>
      </div>

      {/* Modals */}
      <TodoModalComponent
        ref={todoModal}
        todo={selectedTodo}
      />
    </main>
  );
};

export default TodoPage;
