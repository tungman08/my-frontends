import { useFetchInstance } from '@/composables/fetch';
import type { City } from './weather.model';

const fetch = useFetchInstance('/');

export interface WeatherService {
  getCity: (cityName: string) => Promise<City>;
}

const getCity = (cityName: string): Promise<City> =>
  fetch.Get<City>(`/api/weather/${cityName}`);

export default { getCity } as WeatherService;
