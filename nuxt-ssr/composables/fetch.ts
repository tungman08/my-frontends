import { FetchOptions } from 'ohmyfetch';

export interface FetchInstance {
  Get:  <T>(request: string, opts?: FetchOptions) => Promise<T>;
  Post: <T>(request: string, data: unknown, opts?: FetchOptions) => Promise<T>;
  Put: <T>(request: string, data: unknown, opts?: FetchOptions) => Promise<T>;
  Delete: <T>(request: string, opts?: FetchOptions) => Promise<T>;
}

export const useFetchInstance = (uri: string): FetchInstance => {
  const fetchInstance = <T>() =>
    $fetch.create<T>({
      baseURL: uri,
      async onRequest(/* context */) {
        // console.log('[fetch request]', context.request);
      },
      async onRequestError(/* context */) {
        // console.log('[fetch request error]', context.request, context.error);
      },
      async onResponse(context) {
        await Promise.resolve(handleDates(context.response._data));
      },
      async onResponseError(/* context */) {
        // console.log('[fetch response error]', context.request, context.response.status, context.error);
      },
    }
  );

  // eslint-disable-next-line
  const handleDates = (obj: any) => {
    if (obj == null || typeof obj !== 'object') {
      return obj;
    }

    const isIsoDateString = (value: unknown): boolean => {
      const isoDateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d*)?/;
      return !!value && typeof value === 'string' && isoDateFormat.test(value);
    };  

    Object.entries(obj).forEach(([key, value]) => {
      if (isIsoDateString(value)) {
        obj[key] = new Date(value as string);
      } else if (typeof value === 'object') {
        handleDates(value);
      }
    });
  };

  const Get = <T>(request: string, opts?: FetchOptions): Promise<T> => {
    const fetch = fetchInstance<T>();
    return fetch(request, opts);
  };

  const Post = <T>(request: string, data: unknown, opts?: FetchOptions): Promise<T> => {
    const fetch = fetchInstance<T>();
    return fetch(request, { ...opts, method: 'POST', body: JSON.stringify(data) });
  };

  const Put = <T>(request: string, data: unknown, opts?: FetchOptions): Promise<T> => {
    const fetch = fetchInstance<T>();
    return fetch(request, { ...opts, method: 'PUT', body: JSON.stringify(data) });
  };

  const Delete = <T>(request: string, opts?: FetchOptions): Promise<T> => {
    const fetch = fetchInstance<T>();
    return fetch(request, { ...opts, method: 'DELETE' });
  };

  return { Get, Post, Put, Delete } as FetchInstance;
};
