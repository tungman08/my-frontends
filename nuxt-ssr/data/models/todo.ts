import { DataTypes, Model } from 'sequelize';
import sequelize from '../config/sequelize';  
import { Todo } from '@/store/todo/todo.model';

const todoRepository = sequelize.define<Model<Todo>>('todo', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  date: {
    type: DataTypes.DATE,
    allowNull: false,
  },
  isDone: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
  }
}, {
  // Other model options go here
  timestamps: false,
});

export default todoRepository;
