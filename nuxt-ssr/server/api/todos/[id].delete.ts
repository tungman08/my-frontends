import { sendError } from 'h3';
import { NuxtError } from 'nuxt/dist/app/composables';
import todoRepository from '@/data/models/todo';

export default defineEventHandler(async (event) => {
  const { id } = event.context.params;

  try {
    await todoRepository.destroy({ where: { id: id } });

    return null;
  } catch (err) {
    sendError(event, err as NuxtError);
  }
});
