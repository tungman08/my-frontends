import { sendError } from 'h3';
import { NuxtError } from 'nuxt/dist/app/composables';
import todoRepository from '@/data/models/todo';

export default defineEventHandler(async (event) => {
  try {
    const response = await todoRepository.findAll();

    return response;
  } catch (err) {
    sendError(event, err as NuxtError);
  }
});
