import { sendError } from 'h3';
import { NuxtError } from 'nuxt/dist/app/composables';
import todoRepository from '@/data/models/todo';

export default defineEventHandler(async (event) => {
  const { id } = event.context.params;
  const body = await useBody(event);

  try {
    await todoRepository.update({
      name: body.name,
      date: body.date,
      isDone: body.isDone,
    },
    {
      where: { id: id }
    });

    const response = await todoRepository.findByPk(id);

    return response;
  } catch (err) {
    sendError(event, err as NuxtError);
  }
});
