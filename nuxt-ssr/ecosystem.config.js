module.exports = {
  apps: [
    {
      name: 'NuxtSSR',
      script: './.output/server/index.mjs',
      exec_mode: 'cluster',
      instances: 'max',
      autorestart: true,
      env: {
        PORT: 3000,
        NODE_ENV: 'production'
      }
    }
  ]
};
