import type { NextApiRequest, NextApiResponse } from 'next';
import NextCors from 'nextjs-cors';

const corsOptions = {
  // Options
  origin: '*',
  methods: ['HEAD', 'OPTIONS', 'GET', 'POST', 'PUT', 'DELETE'],
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

export const useCors = (req: NextApiRequest, res: NextApiResponse) => {
  return NextCors(req, res, corsOptions);
};
