import { ActionReducerMapBuilder, createAsyncThunk, createSlice, isFulfilled, isPending, isRejected, PayloadAction } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import type { RootState, Status } from '../';
import weatherService from './weatherService';
import { City } from './weatherModel';

// namespace
export const WEATHER_STORE_NAMESPACE = 'weatherStore';

// state
interface WeatherState {
  city: City;
  status: Status;
}

const initialState: WeatherState = {
  city: {} as City,
  status: 'idle'
};

// reducer
const reducers = {
  load: (state: WeatherState, action: PayloadAction<City>) => {
    state.city = action.payload;
  },
};

// extra reducer
const getCity = createAsyncThunk(`${WEATHER_STORE_NAMESPACE}/getCity`, async (cityName: string, { rejectWithValue }) => {
  try {
    const respone = await weatherService.getCity(cityName);
    return respone.data;
  } catch (err) {
    return rejectWithValue(err);
  }
});

// mapper
const actionReducerMap = (builder: ActionReducerMapBuilder<WeatherState>) => {
  builder
    .addCase(getCity.fulfilled, (state, action) => {
      state.city = action.payload;
    })
    .addCase(getCity.rejected, () => {
      toast.error('Oops, we can\'t find that city');
    })
    .addMatcher(isFulfilled(getCity), (state) => {
      state.status = 'idle';
    })
    .addMatcher(isPending(getCity), (state) => {
      state.status = 'loading';
    })
    .addMatcher(isRejected(getCity), (state) => {
      state.status = 'failed';
    });
};

// slice
const WeatherSlice = createSlice({
  name: WEATHER_STORE_NAMESPACE,
  initialState,
  reducers: reducers,
  extraReducers: (builder) => actionReducerMap(builder)
});

// selector
export const WeatherSelector = {
  city: (state: RootState) => state.weatherStore.city,
  status: (state: RootState) => state.weatherStore.status,
};

// action
export const WeatherAction = {
  load: WeatherSlice.actions.load,
  getCity: (cityName: string) => getCity(cityName),
};

export default WeatherSlice.reducer;
