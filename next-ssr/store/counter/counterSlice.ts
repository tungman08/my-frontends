import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../';

// namespace
export const COUNTER_STORE_NAMESPACE = 'counterStore';

// state
interface CounterState {
  counter: number;
}

const initialState: CounterState = {
  counter: 0,
};

// reducer
const reducers = {
  increment: (state: CounterState) => {
    state.counter += 1;
  },
  decrement: (state: CounterState) => {
    state.counter -= 1;
  },
  setAmount: (state: CounterState, action: PayloadAction<number>) => {
    state.counter = action.payload;
  },
};

// slice
const counterSlice = createSlice({
  name: COUNTER_STORE_NAMESPACE,
  initialState,
  reducers,
});

// selector
export const CounterSelector = {
  counter: (state: RootState) => state.counterStore.counter,
};

// action
export const CounterAction = {
  increment: counterSlice.actions.increment,
  decrement: counterSlice.actions.decrement,
  setAmount: counterSlice.actions.setAmount,
};

export default counterSlice.reducer;
