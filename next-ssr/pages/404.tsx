import Head from 'next/head';

const NotFoundPage = (): JSX.Element => {
  return (<>
    <Head>
      <title>Error</title>
    </Head>
    <div className={'page404 d-flex justify-content-center align-items-center'}>
      <div className={'card card-tranparent'}>
        <div className="card-body text-center px-5 py-4">
          <h1 className={'display-3 mr-4 page404-title'}>404</h1>
          <h4 className="pt-3">Oops! You&apos;re lost.</h4>
          <p className="text-muted float-left">
            The page you are looking for was not found.
          </p>
          <a href="#" onClick={(e) => { e.preventDefault(); history.go(-1); }}>Go Back</a>
        </div>
      </div>
    </div>
    <style jsx>{`
      .page404 {
        min-height: 100vh;
        background-color: white;
      }

      .page404-title {
        color: #dc3545;
        font-weight: bold;
      }

      .card-tranparent {
        background-color: rgba(0, 0, 0, 0.2) !important;
      }
    `}</style>
  </>);
};

export default NotFoundPage;
