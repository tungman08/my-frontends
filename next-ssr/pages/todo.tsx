import type { GetServerSideProps, NextPage } from 'next';
import React from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import absoluteUrl from 'next-absolute-url';
import { useAppDispatch } from '@/hooks/redux';
import { TodoAction } from '@/store/todo/todoSlice';
import { Todo } from '@/store/todo/todoModel';
import type { Ref } from '@/components/modal/ref';
import LayoutComponent from '@/components/layout/LayoutComponent';
import TodoModalComponent from '@/components/todo/TodoModalComponent';

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const { origin } = absoluteUrl(req);
  const response = await fetch(`${origin}/api/todos`);
  const data: Todo[] = await response.json();

  return { 
    props: { 
      data:  data.map(todo => {
        todo.date = new Date(todo.date);
        return todo;
      })
    }
  };
};

const TodoPage: NextPage<{ data: Todo[] }> = ({ data }) => {
  const title = 'Todo';
  const TodoTableComponent = dynamic(() => import('@/components/todo/TodoTableComponent'));
  const todoModal = React.useRef<Ref>(null);

  const dispatch = useAppDispatch();
  const [selectedTodo, setSelectTodo] = React.useState<Todo>();

  React.useEffect(() => {
    dispatch(TodoAction.load(data));
  }, [data]);

  const openCreateModal = () => {
    setSelectTodo(undefined);
    todoModal.current?.show();
  };

  const openEditModal = (todo: Todo) => {
    setSelectTodo(todo);
    todoModal.current?.show();    
  };

  return (<>
    <Head>
      <title>{`${title} - Next.js`}</title>
    </Head>
    <LayoutComponent>
      <main>
        <div className="container my-4">
          <div className="card">
            <div className="card-body">
              <button
                className="btn btn-primary"
                onClick={openCreateModal}
              >
                <span className="fas fa-plus me-2"></span>Add
              </button>

              <TodoTableComponent onSelected={openEditModal} />
            </div>
          </div>

          { /* Modals */ }
          <TodoModalComponent
            ref={todoModal}
            todo={selectedTodo}
          /> 
        </div>
      </main>
    </LayoutComponent>
  </>);
};

export default TodoPage;
