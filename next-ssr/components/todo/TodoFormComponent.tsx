import React from 'react';
import styles from './TodoFormComponent.module.scss';
import { Todo } from '@/store/todo/todoModel';
import { TodoSchema } from '@/store/todo/todoValidator';
import DatePicker, { registerLocale } from 'react-datepicker';
import th from 'date-fns/locale/th';

export type Ref = {
  validate: () => Todo | false,
}

type Props = {
  todo?: Todo;
}

const TodoFormComponent = React.forwardRef<Ref, Props>((props, ref) => {
  React.useImperativeHandle(ref, () => ({
    validate: handleValidate,
  }));

  // register th locale
  registerLocale('th-TH', th);

  React.useEffect(() => {
    if (props.todo) {
      setFormData({
        id: props.todo.id,
        name: props.todo.name,
        date: props.todo.date,
        isDone: props.todo.isDone,
      });
    } 
  }, [props.todo]);

  const [formData, setFormData] = React.useState<Todo>({
    name: '',
    date: new Date(),
    isDone: false,
  });
  const [errors, setErrors] = React.useState({
    name: false,
    date: false,
  });

  const handleValidate = () => {
    const validator = TodoSchema.validate(formData, { abortEarly: false }).error;
    const name = validator ? validator.details.some((detail) => detail.path[0] === 'name') : false;
    const date = validator ? validator.details.some((detail) => detail.path[0] === 'date') : false;

    setErrors({
      ...errors,
      name,
      date,
    });

    return !validator
      ? formData
      : false;
  };

  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      name: e.target.value.trim(),
    });
    clearErrors();
  };

  const handleDateChange = (selectedDate: Date | null) => {
    if (selectedDate) {
      setFormData({
        ...formData,
        date: selectedDate,
      });
      clearErrors();
    }
  };

  const handleIsDoneChange = () => {
    setFormData({
      ...formData,
      isDone: !formData.isDone,
    });
    clearErrors();
  };

  const clearErrors = () => {
    setErrors({
      name: false,
      date: false,
    });
  };

  return (<>
    {/* name */}
    <fieldset className="form-group mb-3">
      <label htmlFor="name">Todo:</label>
      <input
        type="text"
        className={`form-control ${errors.name ? 'is-invalid' : ''}`}
        aria-label="name"
        autoComplete="off"
        value={formData.name}
        onChange={handleNameChange}
      />
      {errors.name &&
        <div
          className="invalid-feedback"
          role="alert"
        >
          <strong>Todo is required.</strong>
        </div>
      }
    </fieldset>

    {/* date */}
    <fieldset className="form-group">
      <label htmlFor="date">Date:</label>
      <DatePicker
        selected={formData.date}
        onChange={(date) => handleDateChange(date)}
        dateFormat="dd/MM/yyyy"
        className="form-control mb-3"
        popperPlacement="bottom"
        locale="th-TH"
      />
      {errors.date &&
        <div
          className="invalid-feedback"
          role="alert"
        >
          <strong>Date is required.</strong>
        </div>
      }
    </fieldset>

    {/* isDone */}
    <fieldset className="form-group">
      <div className="form-check form-switch">
        <input
          type="checkbox"
          id="isDone"
          className="form-check-input"
          checked={formData.isDone}
          onChange={handleIsDoneChange}
        />
        <label className="form-check-label" htmlFor="isDone">Done</label>
      </div>
    </fieldset>
  </>);
});

TodoFormComponent.displayName = 'TodoForm';
export default TodoFormComponent;
