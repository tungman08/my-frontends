import React from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { ToastContainer } from 'react-toastify';
import ActiveLinkComponent from './ActiveLinkComponent';
import logoAlt from '@/assets/images/logo-alt.svg';

type Props = {
  children: JSX.Element,
};

const LayoutComponent = (props: Props): JSX.Element => {
  return (<>
    <div className="layout">
      <header>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container-fluid">
            <Link href="/">
              <a className="navbar-brand d-flex align-items-center">
                <div className="d-inline-flex align-top align-items-center me-2">
                  <div className="invert">
                    <Image
                      src={logoAlt}
                      width={30}
                      height={30}
                      alt="logo"
                    />
                  </div>
                </div>Next.js
              </a>
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarText"
              aria-controls="navbarText"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <ActiveLinkComponent activeClassName="active" href="/">
                    <a className="nav-link">Home</a>
                  </ActiveLinkComponent>
                </li>
                <li className="nav-item">
                  <ActiveLinkComponent activeClassName="active" href="/city">
                    <a className="nav-link">City</a>
                  </ActiveLinkComponent>
                </li>
                <li className="nav-item">
                  <ActiveLinkComponent activeClassName="active" href="/todo">
                    <a className="nav-link">Todo</a>
                  </ActiveLinkComponent>
                </li>
                <li className="nav-item">
                  <ActiveLinkComponent activeClassName="active" href="/about">
                    <a className="nav-link">About</a>
                  </ActiveLinkComponent>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      {props.children}
      <ToastContainer position="bottom-right" theme="colored" />
    </div>
    <style jsx>{`
      .layout {
        width: 100vw;
        min-height: 100vh;
      }

      .invert {
        filter: invert(100%);
      }
    `}</style>
  </>);
};

export default LayoutComponent;
