import React from 'react';
import Image from 'next/image';
import { useAppSelector } from '@/hooks/redux';
import { WeatherSelector } from '@/store/weather/weatherSlice';
import type { City } from '@/store/weather/weatherModel';

type Props = {
  city: City,
  onSearch: React.Dispatch<React.SetStateAction<string>>
};

const WeatherComponent = (props: Props): JSX.Element => {
  const status = useAppSelector(WeatherSelector.status);
  const today = new Date();
  const [input, setInput] = React.useState<string>('');

  const searchClick = () => {
    if (input.length !== 0) {
      const formatedSearch = input.trim().toLowerCase().replace(/\s\s+/g, ' ').split(' ').join('+');
      setInput('');
      props.onSearch(formatedSearch);
    }
  };

  const weatherIcon = (weathers?: Array<{ id: number; main: string; description: string; icon: string; }>) => {
    const icon = (weathers)
      ? (weathers.length !== 0)
        ? weathers[0].icon
        : '01d'
      : '01d';
    return `https://openweathermap.org/img/wn/${icon}@4x.png`;
  };

  const weatherName = (weathers?: Array<{ id: number; main: string; description: string; icon: string; }>) => {
    return (weathers)
      ? (weathers.length !== 0)
        ? weathers[0].main
        : ''
      : '';
  };

  return (<>
    <div className="panel">
      <div className="d-flex justify-content-between">
        <div>
          <h1>{props.city.name}</h1>
          <p className="fs-5 mt-2">{today.toLocaleDateString('en-US', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })}</p>
        </div>
        <div>
          <p className="fs-1">{props.city.main?.temp}°</p>
        </div> 
      </div>
      <div className="text-center" style={{height: '200px'}}>
        {props.city?.weather?.length > 0 &&
          <Image
            src={weatherIcon(props.city.weather)}
            width={200}
            height={200}
            alt="weather icon"
            title={weatherName(props.city.weather)}
          />
        }
      </div>
      <div className="mt-4">
        <div className="input-group">
          <input
            type="text"
            className="form-control"
            placeholder="Search a city..."
            value={input}
            onChange={(e) => setInput(e.target.value)}
            onKeyUp={(e) => { if (e.key === 'Enter') searchClick(); }}
          />
          <button
            type="button"
            className="btn btn-primary"
            onClick={searchClick}
            disabled={status === 'loading'}
          >
            <span className="fa-solid fa-search me-2"></span>Search
          </button>
        </div>
      </div>
    </div>
    <style jsx>{`
      .panel {
        background-color: rgba($color: #000000, $alpha: 0.25);
        padding: 2rem;
        border-radius: 20px;
      }    
    `}</style>
  </>);
};

export default WeatherComponent;
