import { render, screen, within } from '@testing-library/react';
import CounterComponent from './CounterComponent';

describe('CounterComponent', () => {
  beforeEach(() => {
    render(<CounterComponent />);
  });

  it('should render the component', () => {
    expect(within(screen.getByText(/Oops, we can't find that city/i))).toBeDefined();
  });

  // it('should increment the counter value when increment is clicked', () => {

  // });

  // it('should decrement the counter value when decrement is clicked', () => {

  // });
});
