import tile from '@/assets/images/tile.png';

const SlidingComponent = (): JSX.Element => {
  return (<>
    <div className="sliding"></div>
    <style jsx>{`
      .sliding {
        width: 300vw;
        height: calc(100vh - 60px);
        background: url(${tile.src}) repeat;
        animation: slide 60s linear infinite;
      }

      @keyframes slide {
        0% {
          transform: translate3d(0, 0, 0);
        }
        100% {
          transform: translate3d(-2180px, 0, 0);
        }
      }
    `}</style>
  </>);
};

export default SlidingComponent;
