declare namespace NodeJS {
  interface ProcessEnv {
    API_KEY: string;
    DB_HOST: string;
    DB_USERNAME: string;
    DB_PASSWORD: string;
  }
}
