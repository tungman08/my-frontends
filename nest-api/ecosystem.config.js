module.exports = {
  apps: [
    {
      name: 'NestAPI',
      script: 'dist/main.js',
      exec_mode: 'cluster',
      instances: 'max',
      autorestart: true,
      env: {
        PORT: 8000,
        NODE_ENV: 'production'
      }
    }
  ]
};
