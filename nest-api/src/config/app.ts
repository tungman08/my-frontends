import { NestApplicationOptions } from '@nestjs/common';

const options: NestApplicationOptions = {
  cors: {
    origin: '*',
    methods: ['HEAD', 'OPTIONS', 'GET', 'POST', 'PUT', 'DELETE']
  },
  logger: ['log'],
};

export default options;