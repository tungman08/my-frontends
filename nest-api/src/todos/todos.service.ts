import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Todo } from '../data/entities/todo.entity';
import { TodoDto } from '../data/dto/todo.dto';

@Injectable()
export class TodosService {
  constructor(@InjectRepository(Todo) private readonly todoRepository: Repository<Todo>) {}

  async fetch(): Promise<Todo[]> {
    return await this.todoRepository.find();
  }

  async find(id: number): Promise<Todo | null> {
    return await this.todoRepository.findOne({ where: { id: id } });
  }

  async create(todo: TodoDto): Promise<Todo> {
    return await this.todoRepository.save(todo);
  }

  async update(id: number, todo: TodoDto): Promise<Todo | null> {
    await this.todoRepository.update(id, todo);
    return await this.todoRepository.findOne({ where: { id: id } });
  }

  async remove(id: number): Promise<DeleteResult> {
    return await this.todoRepository.delete(id);
  }
}
