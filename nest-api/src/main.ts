import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config'; 
import { SwaggerModule } from '@nestjs/swagger';
import { swagger } from './config/swagger';
import { AppModule } from './app.module';
import options from './config/app';

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule, options);
  const configService = app.get(ConfigService);

  const document = SwaggerModule.createDocument(app, swagger);
  SwaggerModule.setup('', app, document);

  await app.listen(configService.get<number>('PORT', 8000));
  console.log(`Application is running on: ${await app.getUrl()}`);
};

bootstrap();
