import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class todo1664101810046 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'todos',
      columns: [
        { 
          name: 'id',
          type: 'bigint',
          isNullable: false,
          isPrimary: true,
          primaryKeyConstraintName: 'PK_Todos',
          isGenerated: true,
          generationStrategy: 'identity'
        },
        { name: 'name', type: 'varchar', isNullable: false },
        { name: 'date', type: 'date', isNullable: false },
        { name: 'isDone', type: 'boolean', isNullable: false }
      ]
    });

    await queryRunner.createTable(table, true);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('todos', true);
  }
}
